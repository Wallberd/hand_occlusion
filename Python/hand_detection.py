import cv2
import mediapipe as mp
import numpy as np
import glob
import os
import matplotlib.pyplot as plt
import server as svr

def calibrate_camera(folder, gridx, gridy):
    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((gridx*gridy,3), np.float32)
    objp[:,:2] = np.mgrid[0:gridx,0:gridy].T.reshape(-1,2)
    # Arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.
    images = glob.glob(folder+'/*.jpg')
    h = 0
    w = 0
    for fname in images:
        img = cv2.imread(fname)
        h = img.shape[0]
        w = img.shape[1]
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray, (gridx,gridy), None)
        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)
            corners2 = cv2.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
            imgpoints.append(corners)

    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)
    mtx[0,:] /= w
    mtx[1,:] /= h

    return mtx

# Construct a 3D point based on depth
def reconstruct(img_point, intrinsic, zOffset):
    recovered_pose = np.zeros((3,1))
    depth = img_point[2] + zOffset
    recovered_pose[0] = (img_point[0]*depth - intrinsic[0, 2]*depth)/intrinsic[0,0]
    recovered_pose[1] = (img_point[1]*depth - intrinsic[1, 2]*depth)/intrinsic[1,1]
    recovered_pose[2] = depth
    return recovered_pose

# Image is RGB already
def hand_detector(intrn, og_img):
    # Copy to make sure operations don't damage up these matrices
    intrinsic = intrn.copy()
    image = og_img.copy()

    mp_drawing = mp.solutions.drawing_utils
    mp_hands = mp.solutions.hands
    # print(image)
    # print(image.shape)
    # # For static images:
    with mp_hands.Hands(
        static_image_mode=True,
        max_num_hands=2,
        min_detection_confidence=0.5) as hands:

        # Flip for handedness
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        results = hands.process(cv2.flip(image, 1))

        # Print handedness and draw hand landmarks on the image
        # print('Handedness:', results.multi_handedness)
        joint_poses = np.zeros((21, 3))
        hands_exists = False

        # Unnormalize intrinsic matrix
        image_height, image_width, _ = image.shape
        intrinsic[0,:] *= image_width
        intrinsic[1,:] *= image_height

        if results.multi_hand_landmarks:
            hands_exists = True
            for hand_landmarks in results.multi_hand_landmarks:

                Zrt_norm = 2
                idx = 0
                for lm in hand_landmarks.landmark:
                    pose = reconstruct([(1-lm.x)*image_width, lm.y*image_height, lm.z], intrinsic, Zrt_norm)
                    joint_poses[idx] = 2*pose.transpose()
                    idx += 1

                    # # Reprojection testin
                    # reproject = intrinsic @ pose
                    # reproject /= reproject[2]
                    #
                    # image = cv2.circle(image, (int(reproject[0]), int(reproject[1])), 3, (0,255,0), -1)

        # Distance between wrist and index finger MCP based on arbitrary pos
        wrst2idxmcp_arbit_size = np.linalg.norm(joint_poses[0] - joint_poses[5])

        # Adjust based on MCP size
        wrst2idxmcp_real_size = 0.074

        # Scale hand pose to real hand positions
        if wrst2idxmcp_arbit_size != 0:
            joint_poses *= wrst2idxmcp_real_size/wrst2idxmcp_arbit_size

        return hands_exists, joint_poses, image

# exists, joint_poses = hand_detector(np.loadtxt("intrinsic"), cv2.imread("test.jpg"))
# print(joint_poses)
#       mp_drawing.draw_landmarks(
#           annotated_image, hand_landmarks, mp_hands.HAND_CONNECTIONS)
    # cv2.imwrite(
    #     '/tmp/annotated_image' + str(idx) + '.png', cv2.flip(annotated_image, 1))

# For webcam input:

# finger_reference_size = 7.18 # cm

# cap = cv2.VideoCapture(0)
# with mp_hands.Hands(
#     min_detection_confidence=0.5,
#     min_tracking_confidence=0.5) as hands:
#   while cap.isOpened():
#     success, image = cap.read()
#     if not success:
#       print("Ignoring empty camera frame.")
#       # If loading a video, use 'break' instead of 'continue'.
#       continue
#
#     # Flip the image horizontally for a later selfie-view display, and convert
#     # the BGR image to RGB.
#     image = cv2.cvtColor(cv2.flip(image, 1), cv2.COLOR_BGR2RGB)
#     # To improve performance, optionally mark the image as not writeable to
#     # pass by reference.
#     image.flags.writeable = False
#     results = hands.process(image)
#
#     # Draw the hand annotations on the image.
#     image.flags.writeable = True
#     image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
#     if results.multi_hand_landmarks:
#       for hand_landmarks in results.multi_hand_landmarks:
#         mp_drawing.draw_landmarks(image, hand_landmarks, mp_hands.HAND_CONNECTIONS)
#
#         print(hand_landmarks.landmark[mp_hands.HandLandmark.INDEX_FINGER_TIP])
#     cv2.imshow('MediaPipe Hands', image)
#     if cv2.waitKey(5) & 0xFF == 27:
#       break
# cap.release()


# plt.show()
