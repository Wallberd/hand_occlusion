import server as svr
import cv2
import numpy as np
import hand_detection

port = 65535
serv = svr.udp_server(port)
serv.start()
while True:
    x = input()

    if x == "quit":
        serv.stop()
        break

    if x == "stop":
        serv.stop()
