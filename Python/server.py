import socket
import threading
import struct
import time
import numpy as np
import sys
import os
from hand_detection import hand_detector
from hand_detection import calibrate_camera
import cv2

import time

# UDP server class
# Facilitates communication for the phone
# Members:
#   port - Server binding port
#   img - Image received
#   is_running - State of the client
class udp_server:
    def __init__(self, port):
        self.__port = port
        self.__is_running_listener = False
        self.__is_running_sender = False
        self.__img = np.zeros((10, 10))

        self.__clientIP = "127.0.0.1"
        self.__clientPort = 65533

        if os.path.exists("intrinsic"):
            self.__intrinsic = np.loadtxt("intrinsic")
        else:
            print("Calibrating camera...")
            self.__intrinsic = calibrate_camera("calibration", 9, 6)
            np.savetxt("intrinsic", self.__intrinsic)
            print("Done calibrating!")
        print("Starting server!")

    # Continually receive data from clients
    def __run_listener(self):
        idx = 0
        while self.__is_running_listener:
            try:
                # time.sleep(1.0/30)
                fullImg = bytearray()
                data, address =self.__ls.recvfrom(65535)
                self.__clientIP = address[0]

                fullImg += bytearray(data)
                # Reshape from flat array to image array
                imgbytes = np.frombuffer(fullImg, dtype=np.uint8)
                self.__img = cv2.imdecode(imgbytes, cv2.IMREAD_UNCHANGED)
                # cv2.imwrite(f'sequence/test{idx}.jpg', self.__img)
                # idx +=1

            except Exception as e:
                print(e)

        self.__ls.close()

    # Continually send data to clients
    def __run_sender(self):
        idx =0
        while self.__is_running_sender:
            # print("waiting")
            try:
                time.sleep(1.0/15)
                # # Obtain pose
                if np.sum(self.__img)!= 0:
                    hand_exists, poses, labels = hand_detector(self.__intrinsic, self.__img)
                    # cv2.imwrite(f'sequence/test{idx}.jpg', labels)
                    # idx +=1

                    data = struct.pack(f'<{3*21}f', *poses.flatten())
                    self.__ss.sendto(data, (self.__clientIP, 65532))
            except Exception as e:
                print(e)

        self.__ss.close()

    # Get the currently stored image
    def get_img(self):
        return self.__img

    # Set the state of the server to stop
    def stop(self):
        self.__is_running_listener = False
        self.__is_running_sender = False

    # Creates a thread to send data
    def start(self):
        if not self.__is_running_listener:
            self.__ls = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.__ls.settimeout(10)
            self.__init_socket(self.__ls, 65535)
            self.__listener_thread = threading.Thread(target = self.__run_listener)
            self.__is_running_listener = True
            self.__listener_thread.start()

        if not self.__is_running_sender:
            self.__ss = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.__ss.settimeout(10)
            self.__init_socket(self.__ss, 65534)
            self.__sender_thread = threading.Thread(target = self.__run_sender)
            self.__is_running_sender = True
            self.__sender_thread.start()

    # Initializes the socket
    def __init_socket(self, socket, port):
        socket.bind(("", port))
