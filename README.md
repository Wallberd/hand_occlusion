# Realistic Hand Occlusion with Unity

![Example](Images/Screenshot_20210502-171700.jpg)
A project to enable hands to occlude AR content. For our pipeline, there are a couple simple steps the program takes to apply occlusion. After calibrating the camera, the data from hand tracking goes to Unity, where it syncs that data up with our model hand's position, moving the model hand to follow the actual hand. From there, the occluder is applied to the model hand so that Unity automatically detects which objects are in the foreground or background and occludes objects depending on their depth, allowing for smooth hand occlusion.

## Running the Application
First in Python, run main.py using
{
python3 main.py
}

This starts the UDP server. Running this requires an "intrinsic" file containing the intrinsic matrix of the camera (see "Calibrating Camera" below). After calibration, you can run the Android application or click the play button on the Unity Editor. Exit the Python server by entering "quit" in the terminal". Shut down the Android App before exiting - sometimes quitting the server first results in all ports on the Android phone becoming blocked, requiring a reboot. You can check if ports are blocked by trying to use the phone to SSH into the server.

### Printing the Marker
The marker that the app uses can be found in the Images folder, "ar_final_target_image.png." Ideally, this should be printed out so that the image becomes 20x20cm. The size of the marker can be adjusted in the Unity Editor if the resulting image is not this size however. Simply set the scale of the ImageTarget to the desired size in meters. 

### Depedencies
The application uses Python 3. Python should have numpy, mediapipe, cv2, and matplotlib installed. On the Unity side, version Unity 2019.4 LTS should be used.

## Configuring Parameters
### Calibrating Camera
A calibrated camera is required to get this to work. Print out the checkerboard pattern found at https://docs.opencv.org/3.1.0/pattern.png and use the camera to take at least 10 images of this at various angles. Calibration images need to be put in a folder called "calibration" in the same directory as all the other Python files. Or if you already know the normalized intrinsic matrix, you can directly edit the "intrinsic" file. Running main.py will automatically start calibration if no intrinsic file is found.

### Adjust hand scale
In hand_detection.py, wrst2idxmcp_real_size also should be edited to match the distance between your index finger's MCP and your wrist point. This measurement does not need to be exact, but should be as close as possible. An image of the example distance is below (the blue line):

![Distance to Measure](Images/hand_crops.png)


### Configuring the IP
To edit the IP of the computer that the device connects to, edit the IP field on the bottom left. If this does not work, you may need to manually edit the IP of the build to the local IP of the computer that is running the Python scripts. In the Unity Editor, edit the Canvas > InputField (TMP) GameObject's TextMeshPro component and edit the "Text" field to the IP of the computer. Also make sure firewalls are off for Python on the computer it is running on, otherwise nothing will happen. 

## Building for Android
In order to build on an Android phone, you must do the following:

1) Follow the above steps in order to set up the server and camera parameters.

2) Open the project in Unity.

3) Go to Files --> Build Settings, then select Android and click "Switch Platform" in the bottom-right corner.

4) To check that SDK for Android was in fact installed, go to Unity --> Preferences. Select "External Tools" on the left. Below "Android", the following boxes should be checked off: A) JDK Installed with Unity, B) Android SDK Tools Installed, C) Android NDK Installed, and D) Gradle Installed with Unity.

5) Go back to Build Settings, and select "Build and Run" in the bottom right hand corner. You will prompted to name the build and save it somewhere on your computer. After the file is built, the app should begin to run on your Android.

After following these steps, you should be able use your Android's camera to detect the image target included in our project. This will cause a virtual object to appear in your phone's camera image, which will be occluded when you run your hand over it.

## Editing the scene
The model that is displayed with the marker can be replaced. Simply import another model and replace the GameObject attached to the ImageTarget GameObject with the new model. 
