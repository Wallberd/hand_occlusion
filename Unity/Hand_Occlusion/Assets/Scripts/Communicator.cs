﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using System.Net.Sockets;
using TMPro;
using UnityEngine.Events;
using System;

[System.Serializable]
public class UpdatePoseEvent : UnityEvent<List<Vector3>> { }

public class Communicator : MonoBehaviour
{

    private PIXEL_FORMAT mPixelFormat = PIXEL_FORMAT.RGB888;
    private string IP = "192.168.0.1";
    //public int port = 65534;
    private float time = 0;
    private ImagePacket img;

    public UpdatePoseEvent updatePose;

    UdpClient sender;
    UdpClient listener;

    // Indicates if Vuforia has the image ready
    private bool imageReady;

    // Indicates if image is being sent
    private bool isSending;

    // Indicate start of the program
    private bool startSocket = false;

    // Force socket off manually
    private bool forceSocketOff = false;
 
    void Start()
    {
        
        #if UNITY_EDITOR
        mPixelFormat = PIXEL_FORMAT.GRAYSCALE; // Need Grayscale for Editor
#else
        mPixelFormat = PIXEL_FORMAT.RGB888; // Use RGB888 for mobile
#endif
        VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted);
        VuforiaARController.Instance.RegisterTrackablesUpdatedCallback(OnTrackablesUpdated);

        // Connect to server
        sender = new UdpClient(65523);
        listener = new UdpClient(65532);
        sender.Connect(IP, 65535);

    }

    private void OnVuforiaStarted()
    {
        // Vuforia has started, now register camera image format
        if (CameraDevice.Instance.SetFrameFormat(mPixelFormat, true))
        {
            Debug.Log("Successfully registered pixel format " + mPixelFormat.ToString());
        }
        else
        {
            Debug.LogError(
              "Failed to register pixel format " + mPixelFormat.ToString() +
              "\n the format may be unsupported by your device;" +
              "\n consider using a different pixel format.");
        }
    }
    void Update()
    {
        time += Time.deltaTime;

        // Give Vuforia time to setup, otherwise not nice things happen
        if (!startSocket)
        {
            if (time > 5f)
            {
                startSocket = true;
            }   
        }

        else if (imageReady && startSocket && !forceSocketOff && !isSending)
        {
            isSending = true;
            try
            {
                SendImage();
                ReceivePoses();
                // Get the actual image data


                //stream.Close();
            }
            catch
            {
                Debug.Log("Failed to send!");
            }

            //socket.Close();
            //socket.Dispose();

            imageReady = false;
            isSending = false;
        }
        Debug.Log(IP);
    }

    async void SendImage()
    {
        try
        {
            byte[] imgBytes = img.imgData;
            await sender.SendAsync(imgBytes, imgBytes.Length);
        }
        catch (ObjectDisposedException)
        {
            Debug.Log("Socket already disposed!");
        }
    }

    async void ReceivePoses()
    {
        try
        {
            // Read the resulting pose
            List<Vector3> joint_pos = new List<Vector3>();
            var receivedResults = await listener.ReceiveAsync();
            byte[] poses = receivedResults.Buffer;

            for (int i = 0; i < 21; i++)
            {
                Vector3 pos = new Vector3();
                for (int j = 0; j < 3; j++)
                {
                    pos[j] = BitConverter.ToSingle(poses, (i * 3 + j) * 4);
                }
                joint_pos.Add(pos);
            }
            updatePose.Invoke(joint_pos);
        }
        catch(ObjectDisposedException)
        {
            Debug.Log("Socket already disposed!");
        }
        //debug.GetComponent<TextMeshPro>().text = "Successful Pose Transfer!";
    }
    void OnTrackablesUpdated()
    {
        if (time > 1.0f/30 && startSocket && !isSending)
        {
            
            // Obtain Vuforia Image
            Vuforia.Image image = CameraDevice.Instance.GetCameraImage(mPixelFormat);
#if UNITY_EDITOR

            Texture2D temp = new Texture2D(1, 1, TextureFormat.Alpha8, false);
            image.CopyToTexture(temp);
            Color32[] colors = temp.GetPixels32();
            for (int i = 0; i < image.Height * image.Width; i++)
            {
                colors[i].r = colors[i].a;
                colors[i].b = colors[i].a;
                colors[i].g = colors[i].a;

            }
            Texture2D imageTex = new Texture2D(image.Width, image.Height, TextureFormat.RGB24, false);
            imageTex.hideFlags = HideFlags.HideAndDontSave;
            imageTex.SetPixels32(colors);
            imageTex.Apply();

#else
                Texture2D imageTex = new Texture2D(1, 1, TextureFormat.RGB24, false);
                imageTex.hideFlags = HideFlags.HideAndDontSave;
                image.CopyToTexture(imageTex);
                // Reduce size to increase transfer speed
                TextureScale.scale(imageTex, image.Width / 2, image.Height / 2);

#endif

            // Create image packet
            img = new ImagePacket();
            img.width = imageTex.width;
            img.height = imageTex.height;
            //img.imgData = temp.GetRawTextureData();
            img.imgData = ImageConversion.EncodeToJPG(imageTex);
            imageReady = true;
            time = 0;
            RenderTexture tempRT = RenderTexture.active;
            RenderTexture.active = null;
            if(tempRT != null)
                RenderTexture.ReleaseTemporary(tempRT);
    
            Destroy(imageTex);
        }
    }

    public void ToggleSocket()
    {
        forceSocketOff = !forceSocketOff;
    }

    public void UpdateIP(string s)
    {
        IP = s;
        if (sender != null)
        {
            sender.Close();
            sender.Dispose();
        }

        sender = new UdpClient(65533);
        sender.Connect(IP, 65535);

    }

    private void OnApplicationQuit()
    {
        if (sender != null)
        {
            sender.Close();
            sender.Dispose();
        }

        if (listener != null)
        {
            listener.Close();
            listener.Dispose();
        }
    }
}
