﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerController : MonoBehaviour
{
    // Count of joitns should match number of finger joints
    public void UpdateFingerJointAngles(List<Vector3> joints)
    {
        Vector3 priorVector = Vector3.right;
        Transform t = transform;
        for(int i = 0; i < joints.Count - 1; i++)
        {
                Vector3 localDirection = t.InverseTransformVector(joints[i + 1] - joints[i]);
                t = t.GetChild(0);
                float angle = Vector3.Angle(priorVector, localDirection);
                t.localRotation = Quaternion.Euler(0, 0, -angle);
                //priorVector = localDirection;
            }
        
    }
}
