﻿using System.Collections;

using System.Collections.Generic;

using TMPro;

using UnityEngine;



public class HandControllTest : MonoBehaviour

{

    public GameObject debug;



    List<Vector3> joints;

    // Start is called before the first frame update

    void Start()

    {

        joints = new List<Vector3>();

        for (int i = 0; i < 21; i++)

        {

            joints.Add(Vector3.back);

        }

    }



    // Update is called once per frame

    void Update()

    {

        int idx = 0;

        string jointStr = "";

        foreach (Transform child in transform)

        {

            jointStr += joints[idx] + "\n";

            child.position = Camera.main.transform.TransformPoint(joints[idx++]);

        }

        //debug.GetComponent<TextMeshPro>().text = jointStr;



    }



    public void UpdateJoints(List<Vector3> new_points)
    {

        for (int i = 0; i < 21; i++)

        {

            joints[i] = new_points[i];

        }

    }

}

