﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HandJointController : MonoBehaviour
{
    public GameObject debug;
    public GameObject debug2;
    public GameObject mesh;

    List<Vector3> joints;
    public List<Transform> riggedJoints;
    Quaternion baseOrientationOffset;

    bool poseAvailable;
    // Start is called before the first frame update
    void Start()
    {
        joints = new List<Vector3>();
        for (int i = 0; i < 21; i++)
        {
            joints.Add(Vector3.zero);
        }

        Quaternion offset = ComputeOrientation(riggedJoints[0].position, riggedJoints[6].position, riggedJoints[21].position).rotation;

        baseOrientationOffset = Quaternion.Inverse(offset*Quaternion.Euler(0,180,0)) * riggedJoints[0].rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (poseAvailable)
        {
            Debug.Log(riggedJoints.Count);
            mesh.GetComponent<Renderer>().enabled = true;
            riggedJoints[0].position = joints[0];
            Matrix4x4 currentOrientation = ComputeOrientation(joints[0], joints[5], joints[17]);
            Matrix4x4 currentModelOrientation = ComputeOrientation(riggedJoints[0].position, riggedJoints[6].position, riggedJoints[21].position);

            riggedJoints[0].rotation = currentOrientation.rotation * baseOrientationOffset;
            riggedJoints[21].position = (joints[5] + joints[0]) / 2;
            riggedJoints[22].position = (joints[9] + joints[0]) / 2;
            riggedJoints[23].position = (joints[13] + joints[0]) / 2;
            riggedJoints[24].position = (joints[17] + joints[0]) / 2;

            RotateFingers();

            // Finger iteration
            for (int f = 1; f < 21; f++)
            {
                riggedJoints[f].position = joints[f];
            }


            //riggedJoints[21].position = (joints[5] + joints[0]) / 2;
            //riggedJoints[22].position = (joints[9] + joints[0]) / 2;
            //riggedJoints[23].position = (joints[13] + joints[0]) / 2;
            //riggedJoints[24].position = (joints[17] + joints[0]) / 2;

            ////Finger iteration refinement
            //for (int f = 1; f < 21; f++)
            //{
            //    riggedJoints[f].position = joints[f];
            //}

            //debug.transform.position = joints[0];
            //debug.transform.rotation = currentOrientation.rotation;
            //debug2.transform.position = joints[0];
        }
        else
        {
            //riggedJoints[0].position = Camera.main.transform.TransformPoint(Vector3.back);
            mesh.GetComponent<Renderer>().enabled = false;
        }
            //debug2.transform.rotation = ComputeOrientation(riggedJoints[0].position, riggedJoints[6].position, riggedJoints[21].position).rotation;
    }

    public void RotateFingers()
    {
        riggedJoints[1].gameObject.GetComponent<FingerController>().UpdateFingerJointAngles(joints.GetRange(1, 4));

        List<Vector3> fingerJoints = joints.GetRange(5, 4);
        fingerJoints.Insert(0, riggedJoints[21].position);
        riggedJoints[21].gameObject.GetComponent<FingerController>().UpdateFingerJointAngles(fingerJoints);

        fingerJoints = joints.GetRange(9, 4);
        fingerJoints.Insert(0, riggedJoints[22].position);
        riggedJoints[22].gameObject.GetComponent<FingerController>().UpdateFingerJointAngles(fingerJoints);

        fingerJoints = joints.GetRange(13, 4);
        fingerJoints.Insert(0, riggedJoints[23].position);
        riggedJoints[23].gameObject.GetComponent<FingerController>().UpdateFingerJointAngles(fingerJoints);

        fingerJoints = joints.GetRange(17, 4);
        fingerJoints.Insert(0, riggedJoints[24].position);
        riggedJoints[24].gameObject.GetComponent<FingerController>().UpdateFingerJointAngles(fingerJoints);
    }

    public void UpdateJoints(List<Vector3> new_points){
        float sum = 0;
        for (int i = 0; i < 21; i++)
        {
            joints[i] = Camera.main.transform.TransformPoint(new_points[i]);
            sum += new_points[i][0] + new_points[i][1] + new_points[i][2];
        }
        poseAvailable = (sum != 0);
    }

    private Matrix4x4 ComputeOrientation(Vector3 palm, Vector3 index, Vector3 pinky)
    {
        Vector3 palm2index = index - palm;
        Vector3 palm2pinky = pinky - palm;

        Vector3[] rot = new Vector3[3];

        // NEEDS TO BE PRECISE OR ELSE .rotation PRODUCES AMBIGUOUS ORIENTATIONS!
        rot[2] = Vector3.Cross(palm2pinky, palm2index).normalized; // Palm Vector as Z
        rot[1] = palm2index.normalized; // Palm to index vector as X
        rot[0] = Vector3.Cross(rot[1], rot[2]).normalized; // Vector perpendicular to the previous two as Y

        Matrix4x4 orientation = Matrix4x4.identity;

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                orientation[j, i] = rot[i][j];

        return orientation;
    }

    private Quaternion ComputePalmOrientation(Matrix4x4 orientaion1, Matrix4x4 orientaion2)
    {
        return (orientaion1.inverse * orientaion2).rotation;
    }
}
