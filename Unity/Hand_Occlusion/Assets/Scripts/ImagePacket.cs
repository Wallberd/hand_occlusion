﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImagePacket
{
    public byte[] imgData {get; set;}
    public int width { get; set; }
    public int height { get; set; }
    public int channels { get; set; }

}
